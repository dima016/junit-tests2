package org.batsunov;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class RectangleTest {
    Rectangle rectangle;


    @Before
    public void setUp() throws Exception {
        rectangle = new Rectangle(15, 10);

    }


    @Test
    public void shodReturnZeroAreaIfWidthOrHeightIsZero() {

        rectangle.setWidth(0);
        //Zero Width
        assertEquals(0, rectangle.getArea());

        rectangle.setHeight(0);
        //Zero Height
        assertEquals(0, rectangle.getArea());
    }


    @Test
    public void shouldReturnCorrectArea() {
        rectangle.setWidth(5);
        rectangle.setHeight(8);

        int correctArea = rectangle.getHeight() * rectangle.getWidth();


        assertEquals(correctArea, rectangle.getArea(), 0.1);
    }

    @Test
    public void shouldReturnCorrectPerimeter() {
        rectangle.setHeight(4);
        rectangle.setWidth(2);

        int correctPerimeter = 2 * rectangle.getHeight() + 2 * rectangle.getWidth();

        assertEquals(correctPerimeter, rectangle.getPerimeter(), 0.1);
    }

    @Test
    public void shouldReturnZeroPerimeterIfWidthAndHeightZero() {

        Rectangle rectangleZeroWidthAndHeight = new Rectangle(0, 0);

        assertEquals(0, rectangleZeroWidthAndHeight.getPerimeter());
    }
}
