package org.batsunov;


import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class TriangleTest {

    Triangle triangle;


    @Before
    public void setTriangles() {
        triangle = new Triangle(10, 35, 60);
    }

    @Test
    public void shouldReturnCorrectPerimeter() {
        int sideC = calculateC();
        int correctPerimeter = (triangle.getA() + triangle.getB() + sideC);

        assertEquals(correctPerimeter, triangle.getPerimeter(), 0.1);
    }

    @Test
    public void shouldReturnCorrectArea() {
        triangle.setA(12);

        int sideC = calculateC();
        int p = triangle.getPerimeter() / 2;
        int correctArea = (int) Math.sqrt(p * (p - triangle.getA()) * (p - triangle.getB()) * (p - sideC));

        assertEquals(correctArea, triangle.getArea(),0.1);

    }

    @Test
    public void shouldReturnCorrectRadiansAngel() {
        assertEquals(Math.toRadians(60), triangle.getAngle(), 0.01);
    }

    @Test
    public void shouldNotReturnZeroIfCorrectDate() {
        assertNotEquals(0, triangle.getArea());
    }

    @Test
    public void shouldReturnSameColorWhichBeInitialize() {
        ColorTriangle colorTriangle = new ColorTriangle(1, 2, 30, "blue");

        assertEquals("blue", colorTriangle.getColor());
    }

    private int calculateC() {
        return (int) Math.sqrt(Math.pow(triangle.getA(), 2) + Math.pow(triangle.getB(), 2) - 2 * triangle.getA() * triangle.getB() * Math.cos(triangle.getAngle()));
    }
}
