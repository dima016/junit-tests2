package org.batsunov;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CircleTest {

    Circle circle;
    ColorCircle colorCircle;

    @Before
    public void setCircles() {
        circle = new Circle(0);
        colorCircle = new ColorCircle(0, "red");
    }

    @Test
    public void shouldReturnZeroIfRadiusIsZero() {

        //test just circle
        assertEquals(0, circle.getArea());
        assertEquals(0, circle.getPerimeter());

        //test Color circle
        assertEquals(0, colorCircle.getArea());
        assertEquals(0, colorCircle.getPerimeter());
    }


    @Test
    public void shouldReturnCorrectPerimeter() {
        circle.setRadius(5);
        colorCircle.setRadius(10);

        int correctPerimeterForCircle = (int) (2 * Math.PI * circle.getRadius());  //формула
        int correctPerimeterForColorCircle = (int) (2 * Math.PI * colorCircle.getRadius());


        //just circle
        assertEquals(correctPerimeterForCircle, circle.getPerimeter(), 0.1);
        // circle with color
        assertEquals(correctPerimeterForColorCircle, colorCircle.getPerimeter(), 0.1);
    }


    @Test
    public void shouldReturnCorrectArea() {
        circle.setRadius(15);
        colorCircle.setRadius(7);

        int correctAreaForCircle = (int) (Math.PI * Math.pow(circle.getRadius(), 2));  //формула
        int correctAreaForColorCircle = (int) (Math.PI * Math.pow(colorCircle.getRadius(), 2));

        assertEquals(correctAreaForCircle, circle.getArea(),0.1);
        assertEquals(correctAreaForColorCircle, colorCircle.getArea(),0.1);
    }


    @Test
    public void shouldReturnSameColorWhichBeInitialize() {

        assertEquals("red", colorCircle.getColor());
    }
}
