package org.batsunov;

import java.util.Objects;

public class ColorCircle extends Circle implements Colorable {

    private String color;

    public ColorCircle(int radius, String color) {
        super(radius);
        this.color = color;
    }

    @Override
    public String getColor() {
        return this.color;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ColorCircle that = (ColorCircle) o;
        return Objects.equals(color, that.color);
    }

    @Override
    public int hashCode() {
        return Objects.hash(color);
    }
}
