package org.batsunov;

public class ColorTriangle extends Triangle implements Colorable {

    private final String color;

    @Override
    public String getColor() {
        return color;
    }

    public ColorTriangle(int a, int b, int angle, String color) {
        super(a, b, angle);
        this.color = color;
    }
}
